## Техника попарного тестирования

| Username                | Password   |
|----------               |----------  |
| standard_user           |secret_sauce|
| locked_out_user         |пустой      |
| problem_user            |неверный    |
| performance_glitch_user |            
| пустой                  |
| неверный                |

## Тест-кейс №1 — Авторизация с помощью Username/Password - standard_user/secret_sauce

1. Предшествующие условия:
    - пользователь находится на странице авторизации https://www.saucedemo.com/
2. Шаги:
    - в поле "Username" ввести "standard_user"
    - в поле "Password" ввести "secret_sauce"
    - нажать на кнопку "LOGIN"
3. Ожидаемый результат:
    - успешная авторизации, переход на сайт интернет-магазина

## Тест-кейс №2 — Авторизация с помощью Username/Password - standard_user/неверный

1. Предшествующие условия:
    - пользователь находится на странице авторизации https://www.saucedemo.com/
2. Шаги:
    - в поле "Username" ввести "standard_user"
    - в поле "Password" ввести "654321"
    - нажать на кнопку "LOGIN"
3. Ожидаемый результат:
    - сообщение об ошибке: Username and password do not match any user in this service

 ## Тест-кейс №3 — Авторизация с помощью Username/Password - standard_user/пусто

1. Предшествующие условия:
    - пользователь находится на странице авторизации https://www.saucedemo.com/
2. Шаги:
    - в поле "Username" ввести "standard_user"
    - поле "Password" оставить пустым
    - нажать на кнопку "LOGIN"
3. Ожидаемый результат:
    - сообщение об ошибке: Password is required 

## Тест-кейс №4 — Авторизация с помощью Username/Password - locked_out_user/неверный

1. Предшествующие условия:
    - пользователь находится на странице авторизации https://www.saucedemo.com/
2. Шаги:
    - в поле "Username" ввести "locked_out_user"
    - в поле "Password" ввести "654321"
    - нажать на кнопку "LOGIN"
3. Ожидаемый результат:
    - сообщение об ошибке: Username and password do not match any user in this service  

## Тест-кейс №5 — Авторизация с помощью Username/Password - locked_out_user/пусто

1. Предшествующие условия:
    - пользователь находится на странице авторизации https://www.saucedemo.com/
2. Шаги:
    - в поле "Username" ввести "locked_out_user"
    - поле "Password" оставить пустым
    - нажать на кнопку "LOGIN"
3. Ожидаемый результат:
    - сообщение об ошибке: Password is required

## Тест-кейс №6 — Авторизация с помощью Username/Password - locked_out_user/secret_sauce

1. Предшествующие условия:
    - пользователь находится на странице авторизации https://www.saucedemo.com/
2. Шаги:
    - в поле "Username" ввести "locked_out_user"
    - в поле "Password" ввести "secret_sauce"
    - нажать на кнопку "LOGIN"
3. Ожидаемый результат:
    - сообщение об ошибке: Sorry, this user has been locked out.

## Тест-кейс №7 — Авторизация с помощью Username/Password - problem_user/пусто

1. Предшествующие условия:
    - пользователь находится на странице авторизации https://www.saucedemo.com/
2. Шаги:
    - в поле "Username" ввести "problem_user"
    - поле "Password" оставить пустым
    - нажать на кнопку "LOGIN"
3. Ожидаемый результат:
    - сообщение об ошибке: Password is required

## Тест-кейс №8 — Авторизация с помощью Username/Password - problem_user/secret_sauce

1. Предшествующие условия:
    - пользователь находится на странице авторизации https://www.saucedemo.com/
2. Шаги:
    - в поле "Username" ввести "problem_user"
    - в поле "Password" ввести "secret_sauce"
    - нажать на кнопку "LOGIN"
3. Ожидаемый результат:
    - успешная авторизации, переход на сайт интернет-магазина

## Тест-кейс №9 — Авторизация с помощью Username/Password - problem_user/неверный

1. Предшествующие условия:
    - пользователь находится на странице авторизации https://www.saucedemo.com/
2. Шаги:
    - в поле "Username" ввести "problem_user"
    - в поле "Password" ввести "654321"
    - нажать на кнопку "LOGIN"
3. Ожидаемый результат:
    - сообщение об ошибке: Username and password do not match any user in this service

## Тест-кейс №10 — Авторизация с помощью Username/Password - performance_glitch_user/неверный

1. Предшествующие условия:
    - пользователь находится на странице авторизации https://www.saucedemo.com/
2. Шаги:
    - в поле "Username" ввести "performance_glitch_user"
    - в поле "Password" ввести "654321"
    - нажать на кнопку "LOGIN"
3. Ожидаемый результат:
    - сообщение об ошибке: Username and password do not match any user in this service

## Тест-кейс №11 — Авторизация с помощью Username/Password - performance_glitch_user/пусто

1. Предшествующие условия:
    - пользователь находится на странице авторизации https://www.saucedemo.com/
2. Шаги:
    - в поле "Username" ввести "performance_glitch_user"
    - поле "Password" оставить пустым
    - нажать на кнопку "LOGIN"
3. Ожидаемый результат:
    - сообщение об ошибке: Password is required

## Тест-кейс №12 — Авторизация с помощью Username/Password - пусто/неверный

1. Предшествующие условия:
    - пользователь находится на странице авторизации https://www.saucedemo.com/
2. Шаги:
    - поле "Username" оставить пустым
    - в поле "Password" ввести "654321"
    - нажать на кнопку "LOGIN"
3. Ожидаемый результат:
    - сообщение об ошибке: Username is required

## Тест-кейс №13 — Авторизация с помощью Username/Password - performance_glitch_user/secret_sauce

1. Предшествующие условия:
    - пользователь находится на странице авторизации https://www.saucedemo.com/
2. Шаги:
    - в поле "Username" ввести "performance_glitch_user"
    - в поле "Password" ввести "secret_sauce"
    - нажать на кнопку "LOGIN"
3. Ожидаемый результат:
    - успешная авторизации, переход на сайт интернет-магазина  

## Тест-кейс №14 — Авторизация с помощью Username/Password - пусто/пусто

1. Предшествующие условия:
    - пользователь находится на странице авторизации https://www.saucedemo.com/
2. Шаги:
    - поле "Username" оставить пустым
    - поле "Password" оставить пустым
    - нажать на кнопку "LOGIN"
3. Ожидаемый результат:
    - сообщение об ошибке: Username is required

## Тест-кейс №15 — Авторизация с помощью Username/Password - пусто/secret_sauce

1. Предшествующие условия:
    - пользователь находится на странице авторизации https://www.saucedemo.com/
2. Шаги:
    - поле "Username" оставить пустым
    - в поле "Password" ввести "secret_sauce"
    - нажать на кнопку "LOGIN"
3. Ожидаемый результат:
    - сообщение об ошибке: Username is required

## Тест-кейс №16 — Авторизация с помощью Username/Password - неверный/пусто

1. Предшествующие условия:
    - пользователь находится на странице авторизации https://www.saucedemo.com/
2. Шаги:
    - в поле "Username" ввести "false"
    - поле "Password" оставить пустым
    - нажать на кнопку "LOGIN"
3. Ожидаемый результат:
    - сообщение об ошибке: Password is required 

## Тест-кейс №17 — Авторизация с помощью Username/Password - неверный/secret_sauce

1. Предшествующие условия:
    - пользователь находится на странице авторизации https://www.saucedemo.com/
2. Шаги:
    - в поле "Username" ввести "false"
    - в поле "Password" ввести "secret_sauce"
    - нажать на кнопку "LOGIN"
3. Ожидаемый результат:
    - сообщение об ошибке: Username and password do not match any user in this service

## Тест-кейс №18 — Авторизация с помощью Username/Password - неверный/неверный

1. Предшествующие условия:
    - пользователь находится на странице авторизации https://www.saucedemo.com/
2. Шаги:
    - в поле "Username" ввести "false"
    - в поле "Password" ввести "654321"
    - нажать на кнопку "LOGIN"
3. Ожидаемый результат:
    - сообщение об ошибке: Username and password do not match any user in this service
        
