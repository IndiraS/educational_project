   ##  Use Cases
 ### Сценарий 1: Получение выгодных предложений
- Как: пользователь сайта https://sbermarket.ru/ с открытым модульным окном авторизации
- Чтобы: получать выгодные предложения интернет-магазина

1. Дано: я нахожусь на странице авторизации
2. Шаги:
- Мне нужно авторизоваться удобным способом
- Я могу прочитать подробнее о предложении нажав на кнопку "выгодные предложения"
- Мне нужно поставить галочку в чек-боксе "Получать выгодные предложения"

3. Когда все параметры будут настроены:
- После авторизации я буду получать выгодные предложения

### Сценарий 2: Вход через VK
- Как: пользователь сайта https://sbermarket.ru/ с открытым модульным окном авторизации
- Чтобы: авторизоваться с помощью соц. сети VK

1. Дано: я нахожусь на странице авторизации
2. Шаги:
- Мне нужно нажать на кнопку "VK"
- В новом окне https://oauth.vk.com/ нажать кнопку "Разрешить"
3. Когда все параметры будут настроены:
- Моя авторизация пройдет успешно

### Сценарий 3: Вход с помощью номера мобильного телефона
- Как: пользователь сайта https://sbermarket.ru/ с открытым модульным окном авторизации
- Чтобы: авторизоваться с помощью номера телефона

1. Дано: я нахожусь на странице авторизации
2. Чтобы просмотреть статистику в модуле аудита:
- Мне нужно ввести в поле "Номер телефона" корректный номер, который начинается с "+7 (9.."
- Мне нужно нажать на кнопку "Получить код в СМС"
- В новом модальном окне, мне нужно ввести код из СМС в поле ввода 
3. Когда все параметры будут настроены:
- Моя авторизация пройдет успешно

### Сценарий 4: Войти по Сбер ID
- Как: пользователь сайта https://sbermarket.ru/ с открытым модульным окном авторизации
- Чтобы: авторизоваться с помощью Сбер ID

1. Дано: я нахожусь на странице авторизации
2. Шаги:
- Мне нужно нажать на кнопку "Войти по Сбер ID"
- В новом окне "Авторизация Сбер ID" ввести номер мобильного телефона
- Мне нужно нажать на кнопку "Войти или создать Сбер ID"
- В новом модальном окне, мне нужно ввести код из СМС в поле ввода, постивить галочку в чек-боксе "Запомнить меня"
3. Когда все параметры будут настроены:
- Моя авторизация пройдет успешно

### Сценарий 5: Вход с помощью почты mail.ru
- Как: пользователь сайта https://sbermarket.ru/ с открытым модульным окном авторизации
- Чтобы: авторизоваться с помощью почты mail.ru

1. Дано: я нахожусь на странице авторизации
2. Шаги:
- Мне нужно нажать на кнопку "@"
- В новом окне https://oauth.mail.ru/ нажать кнопку "Разрешить"
3. Когда все параметры будут настроены:
- Моя авторизация пройдет успешно

### Сценарий 6: Закрытие модального окна авторизации
- Как: пользователь сайта https://sbermarket.ru/ с открытым модульным окном авторизации
- Чтобы: выйти со страницы авторизации

1. Дано: я нахожусь на странице авторизации
2. Шаги:
- Мне нужно нажать на кнопку крестика

3. Когда все параметры будут настроены:
- Модальное окно закроется, я окажусь на главное странице сайта

### Сценарий 7: Ознакомиться с согласием на обработку персональных данных
- Как: пользователь сайта https://sbermarket.ru/ с открытым модульным окном авторизации
- Чтобы: ознакомиться с согласием на обработку персональных данных




1. Дано: я нахожусь на странице авторизации
2. Шаги: 
- Мне нужно нажать на кнопку "с обработкой персональных данных"

3. Когда все параметры будут настроены:
- Я смогу прочитать согласие в новой вкладке
1. Use case: Загрузка большого объема данных
- Цель: Проверить, как система обрабатывает большой объем данных и как быстро она выполняет операции загрузки.
- Шаги:
1. Подготовить тестовые данные большого объема.
2. Загрузить данные в систему.
3. Измерить время, затраченное на загрузку данных.
4. Проверить, что данные были успешно загружены и обработаны системой.
