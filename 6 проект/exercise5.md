# Знакомство с функционалом Swagger
Сайт:  https://fakerestapi.azurewebsites.net/index.html
## 1. Get/api/v1/Activities
HTTP-метод: GET  
Полный URL запроса: https://fakerestapi.azurewebsites.net/api/v1/Activities
Заголовки запроса:-H  "accept: text/plain; v=1.0"  
Тело запроса: отсутствует  
Статус-код ответа: 200  
Тело ответа: 
```
[
  {
    "id": 1,
    "title": "Activity 1",
    "dueDate": "2023-05-30T12:55:12.550936+00:00",
    "completed": false
  },
  ...
  {
    "id": 30,
    "title": "Activity 30",
    "dueDate": "2023-05-31T17:55:12.5509481+00:00",
    "completed": true
  }
]
```
## 2. POST/api/v1/Activities
HTTP-метод: POST  
Полный URL запроса: https://fakerestapi.azurewebsites.net/api/v1/Activities   
Заголовки запроса:-H  "accept: text/plain; v=1.0" -H  "Content-Type: application/json; v=1.0" -d "{\"id\":0,\"title\":\"string\",\"dueDate\":\"2023-05-30T12:12:16.867Z\",\"completed\":true}"  
Тело запроса: 
```
{
  "id": 0,
  "title": "string",
  "dueDate": "2023-05-30T12:12:16.867Z",
  "completed": true
}
```
Статус-код ответа: 200  
Тело ответа: 
```
{
  "id": 0,
  "title": "string",
  "dueDate": "2023-05-30T12:12:16.867Z",
  "completed": true
}
```
## 3. Get/api/v1/Activities/{1}
HTTP-метод: GET  
Полный URL запроса:  https://fakerestapi.azurewebsites.net/api/v1/Activities/1  
Заголовки запроса: -H  "accept: text/plain; v=1.0"   
Тело запроса: отсутствует  
Статус-код ответа: 200  
Тело ответа: 
```
{
  "id": 1,
  "title": "Activity 1",
  "dueDate": "2023-05-30T13:24:11.4111315+00:00",
  "completed": false
}
```
## 4. GET​/api​/v1​/Activities​/{123456789}
HTTP-метод: GET  
Полный URL запроса: https://fakerestapi.azurewebsites.net/api/v1/Activities/123456789  
Заголовки запроса:-H  "accept: text/plain; v=1.0"  
Тело запроса: отсутствует   
Статус-код ответа: 404  
Тело ответа: 
```
{
  "type": "https://tools.ietf.org/html/rfc7231#section-6.5.4",
  "title": "Not Found",
  "status": 404,
  "traceId": "00-30be4531d3e81d45a037412720ecb43b-93aecd07bc452541-00"
}
```

## 5. Get/api/v1/Activities/{123456789987654321}
HTTP-метод: GET  
Полный URL запроса: https://fakerestapi.azurewebsites.net/api/v1/Activities/4321  
Заголовки запроса:-H  "accept: text/plain; v=1.0"  
Тело запроса: отсутствует   
Статус-код ответа: 404  
Тело ответа: 
```
{
  "type": "https://tools.ietf.org/html/rfc7231#section-6.5.1",
  "title": "One or more validation errors occurred.",
  "status": 400,
  "traceId": "00-74be3ec6f9a84e48a70cf6dc0d46fe86-f7cd975081f2bc45-00",
  "errors": {
    "id": [
      "The value '123456789987654321' is not valid."
    ]
  }
}
```
## 6. PUT/api/v1/Activities/{2}
HTTP-метод: PUT  
Полный URL запроса: https://fakerestapi.azurewebsites.net/api/v1/Activities/2  
Заголовки запроса: -H  "accept: text/plain; v=1.0" -H  "Content-Type: application/json; v=1.0" -d "{\"id\":0,\"title\":\"string\",\"dueDate\":\"2023-05-30T12:34:32.452Z\",\"completed\":true}"  
Тело запроса: 
```
{
  "id": 0,
  "title": "string",
  "dueDate": "2023-05-30T12:34:32.452Z",
  "completed": true
}
```
Статус-код ответа: 200  
Тело ответа: 
```
{
  "id": 0,
  "title": "string",
  "dueDate": "2023-05-30T12:34:32.452Z",
  "completed": true
}
```
## 7. PUT/api/v1/Activities/{3333333333333333}
HTTP-метод: PUT  
Полный URL запроса: https://fakerestapi.azurewebsites.net/api/v1/Activities/3333333333333333  
Заголовки запроса: -H  "accept: text/plain; v=1.0" -H  "Content-Type: application/json; v=1.0" -d "{\"id\":0,\"title\":\"string\",\"dueDate\":\"2023-05-30T12:34:32.452Z\",\"completed\":true}"  
Тело запроса: 
```
{
  "id": 0,
  "title": "string",
  "dueDate": "2023-05-30T12:34:32.452Z",
  "completed": true
}
```  
Статус-код ответа: 400  

Тело ответа: 
```
{
  "type": "https://tools.ietf.org/html/rfc7231#section-6.5.1",
  "title": "One or more validation errors occurred.",
  "status": 400,
  "traceId": "00-17c544def4b91646ad6e0df49c1532ad-ae0900f93aca1246-00",
  "errors": {
    "id": [
      "The value '3333333333333333' is not valid."
    ]
  }
}
```
## 8. DELETE​/api​/v1​/Activities​/{3}
HTTP-метод: DELETE  
Полный URL запроса: https://fakerestapi.azurewebsites.net/api/v1/Activities/3   
Заголовки запроса: -H  "accept: */*"   
Тело запроса: отсутствует  
Статус-код ответа: 200  
Тело ответа: отсутсвует

## 9. DELETE​/api​/v1​/Activities​/{987654321987654321}
HTTP-метод: DELETE  
Полный URL запроса: https://fakerestapi.azurewebsites.net/api/v1/Activities/987654321987654321  
Заголовки запроса: -H  "accept: */*"  
Тело запроса: отсутствует   
Статус-код ответа: 400  
Тело ответа: 
```
{
  "type": "https://tools.ietf.org/html/rfc7231#section-6.5.1",
  "title": "One or more validation errors occurred.",
  "status": 400,
  "traceId": "00-b15595cf5e390641991a37611423fedf-b6b41f8ff49a9f47-00",
  "errors": {
    "id": [
      "The value '987654321987654321' is not valid."
    ]
  }
}
```
## 10. GET​/api​/v1​/Authors
HTTP-метод: GET  
Полный URL запроса: https://fakerestapi.azurewebsites.net/api/v1/Authors  
Заголовки запроса: -H  "accept: text/plain; v=1.0"  
Тело запроса: отсутствует  
Статус-код ответа: 200  
Тело ответа: 
```
[
  {
    "id": 1,
    "idBook": 1,
    "firstName": "First Name 1",
    "lastName": "Last Name 1"
  },
  ...
  {
    "id": 610,
    "idBook": 200,
    "firstName": "First Name 610",
    "lastName": "Last Name 610"
  }
]
```
## 11. POST​/api​/v1​/Authors
HTTP-метод: POST  
Полный URL запроса: https://fakerestapi.azurewebsites.net/api/v1/Authors  
Заголовки запроса: -H  "accept: text/plain; v=1.0" -H  "Content-Type: application/json; v=1.0" -d "{\"id\":0,\"idBook\":0,\"firstName\":\"string\",\"lastName\":\"string\"}"  
Тело запроса: 
```
{
  "id": 0,
  "idBook": 0,
  "firstName": "string",
  "lastName": "string"
}
```
Статус-код ответа: 200  
Тело ответа: 
```
{
  "id": 0,
  "idBook": 0,
  "firstName": "string",
  "lastName": "string"
}
```
## 12. GET​/api​/v1​/Authors​/authors​/books​/{11}  
HTTP-метод:GET  
Полный URL запроса: https://fakerestapi.azurewebsites.net/api/v1/Authors/authors/books/11  
Заголовки запроса:-H  "accept: text/plain; v=1.0"  
Тело запроса: отсутствует  
Статус-код ответа: 200  
Тело ответа: 
```
[
  {
    "id": 32,
    "idBook": 11,
    "firstName": "First Name 32",
    "lastName": "Last Name 32"
  },
 ...
  {
    "id": 35,
    "idBook": 11,
    "firstName": "First Name 35",
    "lastName": "Last Name 35"
  }
]
```
## 13. GET​/api​/v1​/Authors​/authors​/books​/{321456987987456}
HTTP-метод: GET  
Полный URL запроса: https://fakerestapi.azurewebsites.net/api/v1/Authors/authors/books/321456987987456  
Заголовки запроса:  -H  "accept: text/plain; v=1.0"  
Тело запроса: отсутствует    
Статус-код ответа: 400  
Тело ответа: 
```
{
  "type": "https://tools.ietf.org/html/rfc7231#section-6.5.1",
  "title": "One or more validation errors occurred.",
  "status": 400,
  "traceId": "00-e8a9292aed68254ba66b3845ecfee4e4-02db1d0753605c41-00",
  "errors": {
    "idBook": [
      "The value '321456987987456' is not valid."
    ]
  }
}
```
## 14. GET​/api​/v1​/Authors​/{22}
HTTP-метод: GET  
Полный URL запроса: https://fakerestapi.azurewebsites.net/api/v1/Authors/22  
Заголовки запроса: -H  "accept: text/plain; v=1.0"  
Тело запроса: отсутствует  
Статус-код ответа: 200  
Тело ответа: 
```
{
  "id": 22,
  "idBook": 8,
  "firstName": "First Name 22",
  "lastName": "Last Name 22"
}
```
## 15. GET​/api​/v1​/Authors​/{2214579863221547}  
HTTP-метод: GET  
Полный URL запроса: https://fakerestapi.azurewebsites.net/api/v1/Authors/2214579863221547    
Заголовки запроса: -H  "accept: text/plain; v=1.0"  
Тело запроса: отсутствует  
Статус-код ответа: 400  
Тело ответа: 
```
{
  "type": "https://tools.ietf.org/html/rfc7231#section-6.5.1",
  "title": "One or more validation errors occurred.",
  "status": 400,
  "traceId": "00-0e9655a81e09a54cbf94fc804c4d03a2-1cf8ed42f8bd1948-00",
  "errors": {
    "id": [
      "The value '2214579863221547' is not valid."
    ]
  }
}
```
## 16. PUT​/api​/v1​/Authors​/{33}
HTTP-метод: PUT    
Полный URL запроса: https://fakerestapi.azurewebsites.net/api/v1/Authors/33    
Заголовки запроса: -H  "accept: text/plain; v=1.0" -H  "Content-Type: application/json; v=1.0" -d "{\"id\":0,\"idBook\":0,\"firstName\":\"string\",\"lastName\":\"string\"}"   
Тело запроса: 
```
{
  "id": 0,
  "idBook": 0,
  "firstName": "string",
  "lastName": "string"
}
```
Статус-код ответа: 200  
Тело ответа: 
```
{
  "id": 0,
  "idBook": 0,
  "firstName": "string",
  "lastName": "string"
}
```
## 17. PUT​/api​/v1​/Authors​/{9632587414587}
HTTP-метод: PUT    
Полный URL запроса: https://fakerestapi.azurewebsites.net/api/v1/Authors/9632587414587
Заголовки запроса: -H  "accept: text/plain; v=1.0" -H  "Content-Type: application/json; v=1.0" -d "{\"id\":0,\"idBook\":0,\"firstName\":\"string\",\"lastName\":\"string\"}"   
Тело запроса: 
```
{
  "id": 0,
  "idBook": 0,
  "firstName": "string",
  "lastName": "string"
}
```
Статус-код ответа: 400  
Тело ответа: 
```
{
  "type": "https://tools.ietf.org/html/rfc7231#section-6.5.1",
  "title": "One or more validation errors occurred.",
  "status": 400,
  "traceId": "00-8160822c59d58f439cd5c7296f54c7cd-529b45ca6dc1c341-00",
  "errors": {
    "id": [
      "The value '9632587414587' is not valid."
    ]
  }
}
```
## 18. DELETE​/api​/v1​/Authors​/{55}
HTTP-метод: DELETE  
Полный URL запроса: https://fakerestapi.azurewebsites.net/api/v1/Authors/55
Заголовки запроса: -H  "accept: */*" 
Тело запроса: отсутствует  
Статус-код ответа: 200  
Тело ответа: отсутсвует

## 19. DELETE​/api​/v1​/Activities​/{556677889988}
HTTP-метод: DELETE  
Полный URL запроса: https://fakerestapi.azurewebsites.net/api/v1/Authors/556677889988  
Заголовки запроса: -H  "accept: */*"  
Тело запроса: отсутствует   
Статус-код ответа: 400  
Тело ответа: 
```
{
  "type": "https://tools.ietf.org/html/rfc7231#section-6.5.1",
  "title": "One or more validation errors occurred.",
  "status": 400,
  "traceId": "00-941b146cf652ce478b74edb10bd2426a-f8bf42c17a81be47-00",
  "errors": {
    "id": [
      "The value '556677889988' is not valid."
    ]
  }
}
```
## 20. GET​/api​/v1​/Books
HTTP-метод: GET  
Полный URL запроса: ttps://fakerestapi.azurewebsites.net/api/v1/Books  
Заголовки запроса: -H  "accept: text/plain; v=1.0"  
Тело запроса: отсутствует  
Статус-код ответа: 200  
Тело ответа: 
```
[
  {
    ""id": 1,
    "title": "Book 1",
    "description": "Lorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\n",
    "pageCount": 100,
    "excerpt": "Lorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\nLorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\nLorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\nLorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\nLorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\n",
    "publishDate": "2023-05-30T06:52:58.5623882+00:00"
  },
  ...
  {
    "id": 200,
    "title": "Book 200",
    "description": "Lorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\n",
    "pageCount": 20000,
    "excerpt": "Lorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\nLorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\nLorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\nLorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\nLorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\n",
    "publishDate": "2022-11-12T06:52:58.5646638+00:00"
  }
]
```
## 21. POST​/api​/v1​/Books
HTTP-метод: POST  
Полный URL запроса: https://fakerestapi.azurewebsites.net/api/v1/Books 
Заголовки запроса: -H  "Content-Type: application/json; v=1.0" -d "{\"id\":0,\"title\":\"string\",\"description\":\"string\",\"pageCount\":0,\"excerpt\":\"string\",\"publishDate\":\"2023-05-31T07:00:28.138Z\"}"  
Тело запроса: 
```
{
  "id": 0,
  "title": "string",
  "description": "string",
  "pageCount": 0,
  "excerpt": "string",
  "publishDate": "2023-05-31T07:00:28.138Z"
}
```
Статус-код ответа: 200  
Тело ответа: 
```
{
  "id": 0,
  "title": "string",
  "description": "string",
  "pageCount": 0,
  "excerpt": "string",
  "publishDate": "2023-05-31T07:00:28.138Z"
}
```
## 22. GET​/api​/v1​/Books​/{12}
HTTP-метод:GET  
Полный URL запроса: https://fakerestapi.azurewebsites.net/api/v1/Books/12  
Заголовки запроса: -H  "accept: text/plain; v=1.0"  
Тело запроса: отсутствует  
Статус-код ответа: 200  
Тело ответа: 
```
{
  "id": 12,
  "title": "Book 12",
  "description": "Lorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\n",
  "pageCount": 1200,
  "excerpt": "Lorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\nLorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\nLorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\nLorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\nLorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\n",
  "publishDate": "2023-05-19T07:02:38.300527+00:00"
}
```
## 23. GET​/api​/v1​/Books​/{654789123987}  
HTTP-метод: GET  
Полный URL запроса: https://fakerestapi.azurewebsites.net/api/v1/Books/654789123987  
Заголовки запроса: -H  "accept: text/plain; v=1.0"  
Тело запроса: отсутствует  
Статус-код ответа: 400  
Тело ответа: 
```
{
  "type": "https://tools.ietf.org/html/rfc7231#section-6.5.1",
  "title": "One or more validation errors occurred.",
  "status": 400,
  "traceId": "00-d75ec3cf336fd048b20e868d9a3f0cfe-5d4a7981a1ea604c-00",
  "errors": {
    "id": [
      "The value '654789123987' is not valid."
    ]
  }
}
```
## 24. PUT​/api​/v1​/Books​/{23}
HTTP-метод: PUT    
Полный URL запроса: https://fakerestapi.azurewebsites.net/api/v1/Books/23      
Заголовки запроса:  -H  "accept: */*" -H  "Content-Type: application/json; v=1.0" -d "{\"id\":0,\"title\":\"string\",\"description\":\"string\",\"pageCount\":0,\"excerpt\":\"string\",\"publishDate\":\"2023-05-31T07:16:38.789Z\"}"   
Тело запроса: 
```
{
  "id": 0,
  "title": "string",
  "description": "string",
  "pageCount": 0,
  "excerpt": "string",
  "publishDate": "2023-05-31T07:16:38.789Z"
}
```
Статус-код ответа: 200  
Тело ответа: 
```
{
  "id": 0,
  "title": "string",
  "description": "string",
  "pageCount": 0,
  "excerpt": "string",
  "publishDate": "2023-05-31T07:16:38.789Z"
}
```
## 25. PUT​/api​/v1​/Books​/{321654987369}
HTTP-метод: PUT    
Полный URL запроса: https://fakerestapi.azurewebsites.net/api/v1/Books/321654987369
Заголовки запроса: -H  "accept: */*" -H  "Content-Type: application/json; v=1.0" -d "{\"id\":0,\"title\":\"string\",\"description\":\"string\",\"pageCount\":0,\"excerpt\":\"string\",\"publishDate\":\"2023-05-31T07:16:38.789Z\"}"   
Тело запроса: 
```
{
  "id": 0,
  "title": "string",
  "description": "string",
  "pageCount": 0,
  "excerpt": "string",
  "publishDate": "2023-05-31T07:16:38.789Z"
}
```
Статус-код ответа: 400  
Тело ответа: 
```
{
  "type": "https://tools.ietf.org/html/rfc7231#section-6.5.1",
  "title": "One or more validation errors occurred.",
  "status": 400,
  "traceId": "00-c4099db4581b7e448319ed27387b6ff5-c5407faf81dbd34a-00",
  "errors": {
    "id": [
      "The value '321654987369' is not valid."
    ]
  }
}
```
## 26. DELETE​/api​/v1​/Books​/{14}
HTTP-метод: DELETE  
Полный URL запроса: https://fakerestapi.azurewebsites.net/api/v1/Books/14
Заголовки запроса: -H  "accept: */*" 
Тело запроса: отсутствует  
Статус-код ответа: 200  
Тело ответа: отсутсвует

## 27. DELETE​/api​/v1​/Books​/{852147963852}
HTTP-метод: DELETE  
Полный URL запроса: https://fakerestapi.azurewebsites.net/api/v1/Books/852147963852  
Заголовки запроса: -H  "accept: */*"  
Тело запроса: отсутствует   
Статус-код ответа: 400  
Тело ответа: 
```
{
  "type": "https://tools.ietf.org/html/rfc7231#section-6.5.1",
  "title": "One or more validation errors occurred.",
  "status": 400,
  "traceId": "00-a6cd34d22f42154ab19a13ac9fb092aa-9c20611a58d3924f-00",
  "errors": {
    "id": [
      "The value '852147963852' is not valid."
    ]
  }
}
```
## 28. GET​/api​/v1​/CoverPhotos
HTTP-метод: GET  
Полный URL запроса: https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos
Заголовки запроса: -H  "accept: text/plain; v=1.0"  
Тело запроса: отсутствует  
Статус-код ответа: 200  
Тело ответа: 
```
[
  {
    "id": 1,
    "idBook": 1,
    "url": "https://placeholdit.imgix.net/~text?txtsize=33&txt=Book 1&w=250&h=350"
  },
  ...
  {
    "id": 200,
    "idBook": 200,
    "url": "https://placeholdit.imgix.net/~text?txtsize=33&txt=Book 200&w=250&h=350"
  }
]
```
## 29. POST​/api​/v1​/CoverPhotos
HTTP-метод: POST  
Полный URL запроса: https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos  
Заголовки запроса: -H  "accept: text/plain; v=1.0" -H  "Content-Type: application/json; v=1.0" -d "{\"id\":0,\"idBook\":0,\"url\":\"string\"}"   
Тело запроса: 
```
{
  "id": 0,
  "idBook": 0,
  "url": "string"
}
```
Статус-код ответа: 200  
Тело ответа: 
```
{
  "id": 0,
  "idBook": 0,
  "url": "string"
}
```
## 30. GET​/api​/v1​/CoverPhotos​/books​/covers​/{32}
HTTP-метод: GET  
Полный URL запроса: https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos/books/covers/32  
Заголовки запроса: -H  "accept: text/plain; v=1.0"  
Тело запроса: отсутствует  
Статус-код ответа: 200  
Тело ответа: 
```
{
  "id": 32,
    "idBook": 32,
    "url": "https://placeholdit.imgix.net/~text?txtsize=33&txt=Book 32&w=250&h=350"
}
```
## 31. GET​/api​/v1​/CoverPhotos​/books​/covers​/{112233445566}  
HTTP-метод: GET  
Полный URL запроса: https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos/books/covers/112233445566  
Заголовки запроса: -H  "accept: text/plain; v=1.0"  
Тело запроса: отсутствует  
Статус-код ответа: 400  
Тело ответа: 
```
{
    "type": "https://tools.ietf.org/html/rfc7231#section-6.5.1",
  "title": "One or more validation errors occurred.",
  "status": 400,
  "traceId": "00-cc747451ed82e74989cf0234d08558d6-4e95cee0be2e894b-00",
  "errors": {
    "idBook": [
      "The value '112233445566' is not valid."
    ]
  }
}
```
## 32. PUT​/api​/v1​/CoverPhotos​/{25}
HTTP-метод: PUT    
Полный URL запроса: https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos/25      
Заголовки запроса:  -H  "accept: text/plain; v=1.0" -H  "Content-Type: application/json; v=1.0" -d "{\"id\":0,\"idBook\":0,\"url\":\"string\"}"   
Тело запроса: 
```
{
  "id": 0,
  "idBook": 0,
  "url": "string"
}
```
Статус-код ответа: 200  
Тело ответа: 
```
{
  "id": 0,
  "idBook": 0,
  "url": "string"
}
```
## 33. PUT​/api​/v1​/CoverPhotos​/{74185296312356}
HTTP-метод: PUT    
Полный URL запроса: https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos/74185296312356  
Заголовки запроса: -H  "accept: text/plain; v=1.0" -H  "Content-Type: application/json; v=1.0" -d "{\"id\":0,\"idBook\":0,\"url\":\"string\"}"  
Тело запроса: 
```
{
  "id": 0,
  "idBook": 0,
  "url": "string"
}
```
Статус-код ответа: 400  
Тело ответа: 
```
{
  "type": "https://tools.ietf.org/html/rfc7231#section-6.5.1",
  "title": "One or more validation errors occurred.",
  "status": 400,
  "traceId": "00-3e8ff9f131204e45bdabb30486ca8391-01144c3f13da2648-00",
  "errors": {
    "id": [
      "The value '74185296312356' is not valid."
    ]
  }
}
```
## 34. DELETE​/api​/v1​/CoverPhotos​/{20}
HTTP-метод: DELETE  
Полный URL запроса: https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos/20
Заголовки запроса: -H  "accept: */*" 
Тело запроса: отсутствует  
Статус-код ответа: 200  
Тело ответа: отсутсвует

## 35. DELETE​/api​/v1​/CoverPhotos​/{20304050607080}
HTTP-метод: DELETE  
Полный URL запроса: https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos/20304050607080  
Заголовки запроса: -H  "accept: */*"  
Тело запроса: отсутствует   
Статус-код ответа: 400  
Тело ответа: 
```
{
  "type": "https://tools.ietf.org/html/rfc7231#section-6.5.1",
  "title": "One or more validation errors occurred.",
  "status": 400,
  "traceId": "00-c35a329507021949a9c30f4d16b69aa6-57e3d153ea2f0d4d-00",
  "errors": {
    "id": [
      "The value '20304050607080' is not valid."
    ]
  }
}
```
## 36. GET​/api​/v1​/Users
HTTP-метод: GET  
Полный URL запроса: https://fakerestapi.azurewebsites.net/api/v1/Users  
Заголовки запроса: -H  "accept: text/plain; v=1.0"   
Тело запроса: отсутствует  
Статус-код ответа: 200  
Тело ответа: 
```
[
  {
    "id": 1,
    "userName": "User 1",
    "password": "Password1"
    },
  ...
  {
    "id": 10,
    "userName": "User 10",
    "password": "Password10"
  }
]
```
## 37. POST​/api​/v1​/Users
HTTP-метод: POST  
Полный URL запроса: https://fakerestapi.azurewebsites.net/api/v1/Users  
Заголовки запроса: -H  "accept: */*" -H  "Content-Type: application/json; v=1.0" -d "{\"id\":0,\"userName\":\"string\",\"password\":\"string\"}"  
Тело запроса: 
```
{
  "id": 0,
  "userName": "string",
  "password": "string"
}
```
Статус-код ответа: 200  
Тело ответа: 
```
{
  "id": 0,
  "userName": "string",
  "password": "string"
}
```
## 38. GET​/api​/v1​/Users​/{2}
HTTP-метод: GET  
Полный URL запроса: https://fakerestapi.azurewebsites.net/api/v1/Users/2   
Заголовки запроса: -H  "accept: */*"   
Тело запроса: отсутствует  
Статус-код ответа: 200  
Тело ответа: 
```
{
  "id": 2,
  "userName": "User 2",
  "password": "Password2"
}
```
## 39. GET​/api​/v1​/Users​/{22}
HTTP-метод: GET  
Полный URL запроса: https://fakerestapi.azurewebsites.net/api/v1/Users/22   
Заголовки запроса: -H  "accept: */*"   
Тело запроса: отсутствует  
Статус-код ответа: 404  
Тело ответа: 
```
{
  "type": "https://tools.ietf.org/html/rfc7231#section-6.5.4",
  "title": "Not Found",
  "status": 404,
  "traceId": "00-36efc27ec21ca34193941bf994214ae1-023b4f80a6058843-00"
}
```
## 40. GET​/api​/v1​/Users​/{123654789963} 
HTTP-метод: GET  
Полный URL запроса: https://fakerestapi.azurewebsites.net/api/v1/Users/123654789963   
Заголовки запроса: -H  "accept: */*"   
Тело запроса: отсутствует  
Статус-код ответа: 400  
Тело ответа: 
```
{
   "type": "https://tools.ietf.org/html/rfc7231#section-6.5.1",
  "title": "One or more validation errors occurred.",
  "status": 400,
  "traceId": "00-8fad4995f40071458c7aa674e142f383-38d3117739b7f04b-00",
  "errors": {
    "id": [
      "The value '123654789963' is not valid."
    ]
  }
}
```
## 41. PUT​/api​/v1​/Users​/{11}
HTTP-метод: PUT    
Полный URL запроса: https://fakerestapi.azurewebsites.net/api/v1/Users/11      
Заголовки запроса:  -H  "accept: */*" -H  "Content-Type: application/json; v=1.0" -d "{\"id\":0,\"userName\":\"string\",\"password\":\"string\"}"   
Тело запроса: 
```
{
  "id": 0,
  "userName": "string",
  "password": "string"
}
```
Статус-код ответа: 200  
Тело ответа: 
```
{
  "id": 0,
  "userName": "string",
  "password": "string"
}
```
## 42. PUT​/api​/v1​/Users​/{885522114477996633}
HTTP-метод: PUT    
Полный URL запроса: https://fakerestapi.azurewebsites.net/api/v1/Users/885522114477996633   
Заголовки запроса:  -H  "accept: */*" -H  "Content-Type: application/json; v=1.0" -d "{\"id\":0,\"userName\":\"string\",\"password\":\"string\"}"    
Тело запроса: 
```
{
  "id": 0,
  "userName": "string",
  "password": "string"
}
```
Статус-код ответа: 400  
Тело ответа: 
```
{
  "type": "https://tools.ietf.org/html/rfc7231#section-6.5.1",
  "title": "One or more validation errors occurred.",
  "status": 400,
  "traceId": "00-5a71a2dbd46a8f42a489ed1b85d9ca09-50a72d60f08c4647-00",
  "errors": {
    "id": [
      "The value '885522114477996633' is not valid."
    ]
  }
}
```
## 43. DELETE​/api​/v1​/Users​/{8}
HTTP-метод: DELETE  
Полный URL запроса: https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos/20
Заголовки запроса: -H  "accept: */*" 
Тело запроса: отсутствует  
Статус-код ответа: 200  
Тело ответа: отсутсвует

## 44. DELETE​/api​/v1​/Users​/{5647932184597}
HTTP-метод: DELETE  
Полный URL запроса: https://fakerestapi.azurewebsites.net/api/v1/Users/5647932184597
Заголовки запроса: -H  "accept: */*"  
Тело запроса: отсутствует   
Статус-код ответа: 400  
Тело ответа: 
```
{
  "type": "https://tools.ietf.org/html/rfc7231#section-6.5.1",
  "title": "One or more validation errors occurred.",
  "status": 400,
  "traceId": "00-3c86c7917cb9fd42b5c6b23d1bef1ceb-c0e0bd88dfb8b84f-00",
  "errors": {
    "id": [
      "The value '5647932184597' is not valid."
    ]
  }
}
```
