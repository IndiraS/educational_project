## Примеры эндпоинтов

Эндпоинт мы будем записывать следующим образом: `{METHOD} /api/{VERSION}/{CLASS}`. 

- METHOD — GET, POST и т.д.
- VERSION — версия API (в нашем случае будем использовать `v1`)
- CLASS — класс объектов, к которому мы обращаемся. Обычно они соответствуют названиям классов в программировании или коллекций в базе данных. К примеру, если там указано значение `users`, то эндпоинт `GET /api/v1/users`, вероятно, отвечает за _получение_ списка пользователей. Давай рассмотрим другой эндпоинт: `POST /api/v1/collection`. Скорее всего, он отвечает за добавление какой-то коллекции. 


## Эндпоинты


1. Получить список рецептов новогодних рецептов
```
GET/api/v1/recipes/novogodnie-recepty
```
2. Создать новый рецепт новогоднего торта
```
POST/api/v1/recipes/novogodnie-recepty/yolochka
```
3. Отредактировать рецепт новогоднего торта с id:2
```
PATCH/api/v1/recipes/novogodnie-recepty/yolochka/{2}  
```
4. Удалить из списка рецептов новогодних блюд торт с id:2
```
DELETE/api/v1/recipes/novogodnie-recepty/yolochka/{2} 
Полный URL запроса: https://www.gastronom.ru/api/v1/recipes/novogodnie-recepty/yolochka/2     
```
