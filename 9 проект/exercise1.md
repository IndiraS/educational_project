# Типы и требования мобильных платформ

## Особенности тестирования мобильных приложений

- Учесть все модели устройств

Разные версии мобильных операционных систем, разрешение экранов и другие характеристики. Важно на старте собрать статистику и понять, какие модели устройств наиболее популярны у конкретных пользователей, и тестировать на этих моделях.

- Тестирование при разных разрешениях

У каждого приложения на платформе Андроид или  IOS есть список разрешений (permissions). Например, разрешения на доступ к файловой системе, местоположению или камере. В зависимости от функционала, приложение запрашивает их у системы. Для успешного тестирования стоит выяснить, при каких действиях приложение запрашивает разрешения, и протестировать эти действия с выданными разрешениями и без них.

- Тестировать разные ОС: самую старую, самую новую ОС и браузеры

Если приложение не поддерживает установленную у пользователя ОС, он не сможет это приложение скачать. Разработчик, зная эту особенность, может поставить заглушку с сообщением, что ОС или браузер нужно обновить.

- Проверить удобство обновлений

Обновление не должно вызывать у пользователя сложностей. При проверке этого кейса нужно выяснить, как будет себя вести приложение, если пользователь не обновил его самостоятельно.

- Тестирование мобильных приложений при различных типах интернет подключения 

Наиболее распространенными типами подключения к интернет для телефонов на Android и iOS является Wifi и 3G(Edge). Именно на этих подключениях обычно тестируют мобильные приложения, которые взаимодействуют с сетью. 

Нужно разобраться, как будет вести себя приложение когда соединение стабильное;  в момент отсутствия сети, например, если пользователь спускается в метро, и сделать так, чтобы у приложения была единая реакция на ситуацию, когда пропадает соединение.

- Проверить взаимодействие с интерфейсом

Надо проверить функциональность, безопасность, удобство, производительность приложения и учесть все детали: от смены размера шрифта и до параллельного использования с другими программами. Можно ли свернуть текущее приложение и зайти в другое? Что ожидается от приложения при таких действиях?

## Инструменты для тестирования мобильных приложений

- Эмуляторы устройств

Для тестирования мобильного приложения нужна ферма устройств, подходящих для целей продукта. Анализируя параметры приложения, собирают матрицу устройств, чтобы охватить большинство комбинаций параметров: разрешение и диагональ экрана, ОС.

Матрица достаточно обширна, но иметь под рукой такое количество устройств излишне. На помощь приходят эмуляторы — приложения, помогающие имитировать работу мобильных устройств с разными параметрами и функциональностью. У iOS — это симулятор Apple iOS, для Android — Android Virtual Device.

- DevTools

Тестировать мобильное веб-приложение можно в обычном браузере на компьютере с помощью инструмента DevTools. Он помогает проверить адаптивность вёрстки, смену ориентации экрана, разные скорости интернет-соединения.

- Сервисы TestFlight и Beta

Активная работа по выявлению ошибок, влияющих на корректную работу приложения, производится на этапе бета-тестирования — использования практически готовой версии перед окончательным запуском. Для бета-тестирования мобильных приложений используют сервисы TestFlight для iOS и Beta для Android.

- Снифферы

Для тестирования взаимодействия с бэкендом — частью приложения, работающей на сервере, — применяют снифферы. Сниффер — это анализатор трафика, то есть всей информации, которая проходит через компьютерные сети. С его помощью можно проверять http-запросы, различные коды ответов и реакцию приложения на них. Самые популярные снифферы — Fiddler и Charles.

## Типы мобильных приложений

- Нативные приложения

Разрабатываются для определённой платформы и устройства и по максимуму используют его возможности.

Нативные приложения сложные и дорогие в разработке, но у них высокая скорость работы, высокая и они позволяют задействовать разные функции телефона — например, камеру, навигатор, список контактов и прочее.

Но есть у подобных приложений и недостатки: низкий по сравнению с мобильными веб-приложениями охват платформ, высокая стоимость разработки и необходимость регулярно выпускать обновления.

- Веб-приложения

Это не приложения, а интерфейсы сайтов, адаптированные под мобильные устройства для удобства пользователей.

Веб-приложения отличаются кросс-платформенностью, простотой и оперативностью в разработке. Но есть у веб-приложений и недостатки: необходимость подключаться к интернету для работы, ограниченная безопасность, невысокая производительность: загрузка файлов и обработка действий пользователя занимают больше времени, чем в нативном приложении.

- Гибридный тип

Комбинация нативного приложения и веба. Разрабатывается сразу для двух платформ и пишется на универсальном языке программирования. Разработка такого приложения дешевле, и за счёт этого оно быстрее выходит на рынок.

Важный плюс — автономное обновление. Для гибридного приложения не нужно постоянно выпускать новую версию, достаточно добавить изменения на сервер.

Один из минусов — слабый визуальный стиль. Приложение пишется сразу для нескольких платформ, поэтому фирменный стиль не адаптируют для каждой из них.

## Различия при тестировании мобильных и веб-приложений

- Самое важное различие при тестировании мобильных и веб-приложений заключается в том, что первое тестирует программные приложения для мобильных устройств, а второе тестирует веб-приложения на предмет функциональности, совместимости и удобства использования. 
- Мобильные приложения могут работать на более широком спектре устройств, такие как смартфоны, умные часы, планшеты, системы блокировки, фитнес-трекеры и планшеты. Это усложняет тестирование мобильных приложений по сравнению с веб-приложениями на мобильных устройствах. 
- Веб-приложения предназначены для стационарных ноутбуков и настольных компьютеров с классическими функциями wifi-маршрутизатора и курсора мыши, которые отсутствуют в случае мобильных приложений.  
- Привычки пользователей сильно изменились в последнее десятилетие. Если раньше устройства часто включались и выключались, а в пользователи входили и выходили из онлайн приложений, то теперь они могут длительное время оставаться в приложениях на своих мобильных устройствах. 
- Мобильные приложения имеют более широкую пользовательскую базу, чем веб-приложения, поэтому тестирование обоих приложений выполняется по-разному с учетом таких факторов, как постоянство доступа к сети, управление уведомлениями, синхронизация приложений между устройствами и так далее.  
- Веб-приложения более ориентированы на бизнес, тогда как мобильные приложения ориентированы на клиента. В результате тестирование мобильных приложений фокусируется на взаимодействии с клиентами и его опыте работы с приложением. 
- Для облачной автоматизации тестирования мобильных и веб-приложений вам потребуется лаборатория мобильных устройств в облаке, а для классических веб-приложений — лаборатория портативных устройств в облаке.
