## **Local Storage**

На примере сайта [OZON](https://www.ozon.ru/category/dom-i-sad-14500/)

|№|Ключ|Значение|Краткое описание|
|-|-|-|-|
|1|lastVisitDate|1543066671525|Дата последнего посещения|
|2|sHist|["селикогель","royal canin","chrome","chromecast","chrome cast","hamax kiss","детский велошлем","hamax","lan"]|История поиска пользователя|
|3|usersCount| [2097] |Количество пользователей|
|4|_dy_device|{"brand":"Other","type":"desktop"}|Информация об используемом устройстве|
|5|_dy_toffset|-1|Проверка отклонения часового пояса|
---

## **Session Storage**

На примере сайта [СберМаркет](https://sbermarket.ru/)


|№|Ключ|Значение|Краткое описание|
|-|-|-|-|
|1|UXS_session_value|300|Время продолжительности сессии|
|2|location|https://sbermarket.ru/magnit_express?referrer=landing_retailer_list&sid=12307| URL страницы, на которой находится пользователь в данный момент |
|3|__vw_tab_guid|8012a6b2-8052-b9d6-84f7-a07462c41a41|Регистрирует данные о поведении посетителей на сайте. Это используется для внутреннего анализа и оптимизации веб-сайта.|
|4|UXS_updated_time|1687250281476|Время с последнего обновления|
|5|AlreadySentABTests|["ab_starting_x_web_non_product_pages_header", "pickup_infoblock_on_landing", "exact_time_web",…]|Проведенные A/B-тестирования|


**Результат выполнения команд для добавления и получения объекта в "Session Storage"**


![1](/materials/3.1_результат.png)

**Состояние "Session Storage" после добавления объекта**

![2](/materials/3.2_состояние.jpg)


**Результат выполнения команд для добавления и получения объекта в "Local Storage"**

![2](/materials/3.3_результат.png)

**Состояние "Local Storage" после добавления объекта**

![2](/materials/3.4_состояние.jpg)





